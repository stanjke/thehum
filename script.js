//Clear top scroll for links with href === #.
document.addEventListener("DOMContentLoaded", function () {
  const links = document.querySelectorAll('a[href^="#"]');
  for (let i = 0; i < links.length; i++) {
    links[i].addEventListener("click", function (e) {
      e.preventDefault();
    });
  }
});

//Our services
const servicesBtns = document.querySelectorAll(".services-btn");
const servicesTabs = document.querySelectorAll(".services-description-wrap");

function onServicesClick(btn) {
  btn.addEventListener("click", function () {
    let btnId = btn.getAttribute("data-tab");
    let currentTab = document.querySelector(btnId);
    if (!currentTab.classList.contains("nav-services-active")) {
      servicesBtns.forEach((e) => e.classList.remove("nav-services-active"));
      servicesTabs.forEach((e) => e.classList.remove("services-active"));
      btn.classList.add("nav-services-active");
      currentTab.classList.add("services-active");
    }
  });
}

servicesBtns.forEach(onServicesClick);

//Our work

const workNavList = document.querySelector(".work-nav-list");
const workNavItems = document.querySelectorAll(".work-nav-btn");
const workList = document.querySelector(".work-list");
const workBtn = document.querySelector(".work-btn");
const workItems = document.querySelectorAll(".work-item");
const workLoader = document.querySelector(".middle-work");

let clickedCount = 0;

function filter(arr, classId) {
  arr.forEach((item) => {
    if (item.classList.contains(classId)) {
      item.style.display = "block";
    } else {
      item.style.display = "none";
    }
  });
}

workNavList.addEventListener("click", (e) => {
  const targetId = e.target.dataset.id;
  const target = e.target;
  const workItems = document.querySelectorAll(".work-item");

  if (target.closest("a")) {
    workNavItems.forEach((item) => item.classList.remove("work-nav-active"));
    target.classList.add("work-nav-active");

    switch (targetId) {
      case "all":
        filter(workItems, "work-item");
        break;
      case "graphic-design":
        filter(workItems, targetId);
        break;
      case "web-design":
        filter(workItems, targetId);
        break;
      case "landing-page":
        filter(workItems, targetId);
        break;
      case "wordpress":
        filter(workItems, targetId);
        break;
    }
  }
});

workBtn.addEventListener("click", function (e) {
  workBtn.classList.remove("active-btn");
  workLoader.classList.add("loader-active");
  setTimeout(() => {
    const duplicateWorkItems = Array.from(workItems).map((item) => item.cloneNode(true));
    clickedCount++;
    duplicateWorkItems.forEach((item) => {
      workList.append(item);
    });
    if (clickedCount >= 2) {
      e.target.closest("a").style.display = "none";
    }
    workLoader.classList.remove("loader-active");
    workBtn.classList.add("active-btn");
  }, 2000);
});

//Carusel
const carusel = document.querySelector(".about-carusel");
const leftArrow = document.querySelector(".arrow-left");
const rightArrow = document.querySelector(".arrow-right");
const quote = document.querySelector(".quote-text");
const figcaption = document.querySelector(".figcaption");

carusel.addEventListener("click", (event) => {
  const titlePic = document.querySelector(".about-pic");
  const caruselPics = Array.from(document.querySelectorAll(".pic-decorate"));
  const clickedImg = event.target.closest("img");

  let currentPos = null;
  let nextPos = null;
  let clickedIndex = null;
  let clickedSrc = null;
  let clickId = null;
  let currentElem = null;

  if (clickedImg) {
    const blockquoteList = document.querySelectorAll(".blockquote-wrap");

    caruselPics.forEach((item) => item.classList.remove("pic-active"));
    titlePic.classList.remove("fade-in");
    titlePic.classList.add("fade-out");

    setTimeout(() => {
      clickedIndex = caruselPics.indexOf(clickedImg);
      clickedSrc = caruselPics[clickedIndex].getAttribute("src");
      clickId = caruselPics[clickedIndex].getAttribute("data-elem");
      currentElem = document.querySelector(clickId);

      if (!currentElem.classList.contains("blockquote-active")) {
        blockquoteList.forEach((e) => e.classList.remove("blockquote-active"));
        currentElem.classList.add("blockquote-active");
      }

      caruselPics[clickedIndex].classList.add("pic-active");
      titlePic.setAttribute("src", clickedSrc);
      titlePic.classList.add("fade-in");
    }, 250);
  }

  if (event.target.classList.contains("arrow-left")) {
    const blockquoteList = document.querySelectorAll(".blockquote-wrap");
    for (let i = 0; i < caruselPics.length; i++) {
      if (caruselPics[i].classList.contains("pic-active")) {
        currentPos = i;
        break;
      }
    }
    caruselPics[currentPos].classList.remove("pic-active");

    titlePic.classList.remove("fade-in");
    titlePic.classList.add("fade-out");

    setTimeout(() => {
      blockquoteList.forEach((e) => e.classList.remove("blockquote-active"));
      if (currentPos === 0) {
        blockquoteList[blockquoteList.length - 1].classList.add("blockquote-active");
        caruselPics[caruselPics.length - 1].classList.add("pic-active");
        nextPos = caruselPics.length - 1;
      } else {
        blockquoteList[currentPos - 1].classList.add("blockquote-active");
        caruselPics[currentPos - 1].classList.add("pic-active");
        nextPos = currentPos - 1;
      }

      let nextPic = caruselPics[nextPos].getAttribute("src");
      titlePic.setAttribute("src", nextPic);
      titlePic.classList.add("fade-in");
    }, 250);
  }

  if (event.target.classList.contains("arrow-right")) {
    const blockquoteList = document.querySelectorAll(".blockquote-wrap");
    for (let i = 0; i < caruselPics.length; i++) {
      if (caruselPics[i].classList.contains("pic-active")) {
        currentPos = i;
        break;
      }
    }

    caruselPics[currentPos].classList.remove("pic-active");
    titlePic.classList.remove("fade-in");
    titlePic.classList.add("fade-out");

    setTimeout(() => {
      blockquoteList.forEach((e) => e.classList.remove("blockquote-active"));
      if (currentPos === 3) {
        blockquoteList[0].classList.add("blockquote-active");
        caruselPics[0].classList.add("pic-active");
        nextPos = 0;
      } else {
        blockquoteList[currentPos + 1].classList.add("blockquote-active");
        caruselPics[currentPos + 1].classList.add("pic-active");
        nextPos = currentPos + 1;
      }
      let nextPic = caruselPics[nextPos].getAttribute("src");

      titlePic.setAttribute("src", nextPic);
      titlePic.classList.add("fade-in");
    }, 250);
  }
});

// mosaic-loader

const gallBtn = document.querySelector(".gallary-btn");
const gallLoader = document.querySelector(".middle-gallary");

gallBtn.addEventListener("click", function () {
  console.log("im working");
  gallBtn.classList.remove("active-btn");
  gallLoader.classList.add("loader-active");
  setTimeout(() => {
    gallLoader.classList.remove("loader-active");
    gallBtn.classList.add("active-btn");
  }, 2000);
});

let masonry;
// masonary
document.addEventListener("DOMContentLoaded", function () {
  // let gridContainer = document.querySelector(".grid");
  masonry = new Masonry("#image-container", {
    itemSelector: ".grid-item",
    columnWidth: 386,
    horizontalOrder: true,
  });
});

// console.dir(Masonry);
// document.addEventListener("DOMContentLoaded", function () {
//   let gridContainer = document.querySelector("#grid-container");
//   var masonry = new Masonry(gridContainer, {
//     itemSelector: ".grid-item",
//     columnWidth: ".grid-sizer",
//     percentPosition: true,
//   });

//   document.querySelector("#add").onclick = function () {
//     // Генерация случайных изображений
//     let imgCount = 7; // Количество изображений, которые нужно добавить
//     let loadedCount = 0; // Счетчик загруженных изображений

//     for (var i = 0; i < imgCount; i++) {
//       let img = new Image();
//       img.onload = function () {
//         // Создание элемента и добавление в сетку Masonry
//         let gridItem = document.createElement("div");
//         gridItem.className = "grid-item";
//         gridItem.appendChild(this); // this ссылается на загруженное изображение

//         gridContainer.appendChild(gridItem);

//         // Обновление раскладки Masonry после добавления изображения
//         masonry.appended(gridItem);

//         // Увеличение счетчика загруженных изображений
//         loadedCount++;

//         // Если все изображения загружены, вызываем layout Masonry
//         if (loadedCount === imgCount) {
//           masonry.layout();
//         }
//       };

//       // Загрузка случайного изображения с помощью picsum.photos
//       img.src = "https://picsum.photos/200/300/?random=" + Math.random();
//     }
//   };
// });

//BUGS :

// - Сделать смену текста, и фигкаптуре.

// -если добавить еще 12 картинок по нажатию Load More то в фильтрацию
//  попадают все добавленные картинки. А нужно сделать так что бы попадали
//  только с нужным классом.|| сделал, но мне кажется это не оптимально решение.
// - Поиграться с галереей и разобраться в масонери.
// -

// let container = document.querySelector("#image-container");
// let statusElem = document.querySelector("#status");
// let progressElem = document.querySelector("progress");

// let loadedImageCount, imageCount;

// document.querySelector("#add").onclick = function () {
// add new images
// let fragment = getItemsFragment();
//   masonry.addItems(fragment);
// container.insertBefore(fragment, container.firstChild);
// container.prepend(fragment);
// masonry.reloadItems();
// masonry.layout();

// use ImagesLoaded
//   let imgLoad = imagesLoaded(container);
//   imgLoad.on("progress", onProgress);
//   imgLoad.on("always", onAlways);
//   // reset progress counter
//   imageCount = imgLoad.images.length;
//   resetProgress();
//   progressElem.setAttribute("value", 0);
// };

// reset container
// document.querySelector("#reset").onclick = function () {
//   container.innerHTML = "";
// };

// -----  ----- //

// function resetProgress() {
//   statusElem.style.opacity = 1;
//   loadedImageCount = 0;
//   progressElem.setAttribute("max", imageCount);
// }

// function updateProgress(value) {}

// // triggered after each item is loaded
// function onProgress(imgLoad, image) {
//   // change class if the image is loaded or broken
//   image.img.parentNode.className = image.isLoaded ? "" : "is-broken";
//   // update progress element
//   loadedImageCount++;
//   progressElem.setAttribute("value", loadedImageCount);
// }

// hide status when done
// function onAlways() {
//   statusElem.style.opacity = 0;
// }

// -----  ----- //

// return doc fragment with
// function getItemsFragment() {
//   let fragment = document.createDocumentFragment();
//   for (let i = 0; i < 9; i++) {
//     let item = getImageItem();
//     fragment.appendChild(item);
//   }
//   return fragment;
// }

// return an <li> with a <img> in it
// function getImageItem() {
//   let item = document.createElement("div");
//   item.className = "grid-item";
//   let img = document.createElement("img");
//   let size = Math.random() * 3 + 1;
//   console.log(size  );
//   let width = Math.random() * 210 + 100;
//   width = Math.round(width * size);
//   let height = Math.round(140 * size);
//   let rando = Math.ceil(Math.random() * 1000);
//   // 10% chance of broken image src
//   // random parameter to prevent cached images
//   let src =
//     rando < 100
//       ? `//foo/broken-${rando}.jpg`
//       : // use picsum.photos for great random images
//         `https://picsum.photos/380/${height}/?${rando}`;
//   img.src = src;
//   item.append(img);
//   return item;
// }
// function getImageItem() {
//   let item = document.createElement("div");
//   item.className = "grid-item";
//   let img = document.createElement("img");
//   let size = Math.random() * 3 + 1;
//   let rando = Math.ceil(Math.random() * 1000000);
//   let src = rando < 100 ? `//foo/broken-${rando}.jpg` : `https://picsum.photos/380/400/?${rando}`; // Используем picsum.photos для генерации случайных изображений с фиксированными размерами
//   img.src = src;
//   img.style.width = "380px"; // Устанавливаем фиксированную ширину изображения
//   //   img.style.height = "400px"; // Устанавливаем фиксированную высоту изображения
//   item.append(img);
//   return item;
// }
